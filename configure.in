dnl Process this file with autoconf to produce a configure script. -*-sh-*-
AC_INIT(include/sane/config.h.in)
AC_CONFIG_HEADER(include/sane/config.h)
# AC_PREREQ(2.10)dnl		dnl Minimum Autoconf version required.
# AC_ARG_PROGRAM

# version code:
V_MAJOR=1
V_MINOR=0
V_BUILD=14
V_EXTRA=-68-g61dd774-dirty

PACKAGE=sane-frontends

GUIPROGS="xscanimage xcam"
BINPROGS="scanadf"

# languages
# ALL_LINGUAS="cs de fr sv it tr"

SANE_V_MAJOR=1
VERSION=${V_MAJOR}.${V_MINOR}.${V_BUILD}${V_EXTRA}
PACKAGE_VERSION="$PACKAGE-$VERSION"
AC_DEFINE_UNQUOTED(PACKAGE, "$PACKAGE", [Define to the name of the distribution.])
AC_DEFINE_UNQUOTED(VERSION, "$VERSION", [Define to the version of the distribution.])
AC_DEFINE_UNQUOTED(PACKAGE_VERSION, "$PACKAGE_VERSION", [Define to the name and the version of the distribution.])
AC_SUBST(PACKAGE)
AC_SUBST(VERSION)
AC_SUBST(PACKAGE_VERSION)
AC_SUBST(SANE_MAJOR)

dnl Check if GUIs shall be compiled
AC_ARG_ENABLE(guis, [  --disable-guis          do not compile graphical user interfaces (GUIs)], USE_GUI=$enableval, USE_GUI=yes)

dnl Check if GTK3 support is wanted
AC_ARG_ENABLE(gtk3, [  --disable-gtk3          do not use GTK3], USE_GTK3=$enableval, USE_GTK3=yes)

dnl Check for gimp plugin support
AC_MSG_CHECKING([whether GIMP plugin is requested])
dnl Default is enabled GIMP plugin
AC_ARG_ENABLE(gimp, [  --disable-gimp          do not include GIMP plugin mode], USE_GIMP=$enableval, USE_GIMP=yes)

AC_MSG_RESULT([$USE_GIMP, GIMP 2.0])

dnl Checks for programs.
AC_PROG_CC
AC_AIX
AC_MINIX
AC_ISC_POSIX
AM_PROG_CC_STDC
AC_PROG_INSTALL
AC_PROG_MAKE_SET
AC_PROG_CPP
AC_PROG_GCC_TRADITIONAL

INCLUDES="${INCLUDES} -I/usr/local/include"
CPPFLAGS="${CPPFLAGS} -D_GNU_SOURCE"
if test "${ac_cv_prog_gcc}" = "yes"; then
  CFLAGS="${CFLAGS} -Wall"
fi

AC_CANONICAL_HOST
dnl special LDFLAGS
case "$host_os" in  
  aix*) #enable .so libraries, disable archives
    LDFLAGS="$LDFLAGS -Wl,-brtl"
    ;;
esac

dnl Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS(os2.h fcntl.h unistd.h libc.h sys/time.h sys/types.h sys/socket.h)

dnl Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_C_INLINE
AC_TYPE_SIGNAL
AC_TYPE_SIZE_T
AC_TYPE_PID_T
AC_CHECK_TYPE(ssize_t, long)
AC_CHECK_TYPE(u_char, unsigned char)
AC_CHECK_TYPE(u_int, unsigned int)
AC_CHECK_TYPE(u_long, unsigned long)
AC_CHECK_TYPE(u_int8_t, unsigned char)
AC_CHECK_TYPE(u_int16_t, unsigned short)
AC_CHECK_TYPE(u_int32_t, unsigned int)

dnl Checks for libraries.
AC_CHECK_LIB(m, sqrt)

if test "`uname`" != "IRIX" -a "`uname`" != "IRIX64"; then
  AC_CHECK_LIB(socket, socket)
  AC_CHECK_LIB(syslog, syslog)            # OS/2 needs this
fi

dnl Checks for library functions.
AC_FUNC_ALLOCA
AC_FUNC_MMAP
AC_CHECK_FUNCS(atexit mkdir sigprocmask strdup strndup strftime strstr \
  strsep strtod snprintf usleep strcasecmp strncasecmp getenv isfdtype vsyslog)

if test "$ac_cv_header_os2_h" = "yes" ; then
  AC_DEFINE(strncasecmp, strnicmp, [Define for OS/2 only])
  AC_DEFINE(strcasecmp, stricmp, [Define for OS/2 only])
fi

AC_CHECK_TOOL(RANLIB, ranlib, :)

PKG_CHECK_MODULES([SANE], [sane-backends >= 1.0.0], HAVE_SANE=yes)
PKG_CHECK_VAR([SANE_PREFIX], [sane-backends >= 1.0.0], [prefix])
PKG_CHECK_VAR([SANE_LDFLAGS], [sane-backends >= 1.0.0], [ldflags])

# check for GTK3
if test "${USE_GUI}" = "yes"; then

    if test "${USE_GTK3}" = "yes" || test "${USE_GIMP}" = "yes"; then
        PKG_CHECK_MODULES([GTK], [gtk+-3.0], HAVE_GTK=yes, [
	    if test "${USE_GIMP}" = "yes"; then
		AC_MSG_WARN([*** GIMP 2.0 plugin requested, but GTK+ 3.0 not found, falling back to GIMP/GTK+ < 3.0 if possible ***])
		USE_GIMP=no
	    else
		AC_MSG_WARN([*** GTK+ 3.0 not found, falling back to GTK+ < 3.0 if possible ***])
	    fi])
    fi

    if test "${USE_GIMP}" = "yes"; then
        PKG_CHECK_MODULES([GIMP], [gimp-2.0], HAVE_GIMP=yes)
	if test "${HAVE_GIMP}" = "yes"; then
	    AC_DEFINE([HAVE_LIBGIMP_GIMP_H], 1, [Define to 1 if you have the <libgimp/gimp.h> header file.])
	fi
    fi

  if test "x${HAVE_GTK}" = "x"; then
    echo "****************************************************************"
    echo "WARNING: GTK-0.99.13 or newer is needed for compiling the GUI"
    echo "         frontends. If you installed gtk as rpm make sure"
    echo "         you also installed gtk-devel."
    echo "    ===> GUIs will be deactivated"
    echo "****************************************************************"
    echo ""
    echo "You may need to remove $cache_file before you run configure again."
    echo ""
  else
    BINPROGS=${BINPROGS}" "${GUIPROGS}
  fi
fi

AC_DEFINE([HAVE_LIBGIMP_GIMP_H], 1, [Define to 1 if you have the <libgimp/gimp.h> header file.])

BINPROGS=${BINPROGS}" "${GUIPROGS}

AC_SUBST(INCLUDES)
AC_SUBST(BINPROGS)
AC_SUBST(GTK_CFLAGS)
AC_SUBST(GTK_LIBS)
AC_SUBST(GIMP_CFLAGS)
AC_SUBST(GIMP_LIBS)
CPPFLAGS="${CPPFLAGS} \
	  -DPATH_SANE_DATA_DIR=\$(sanedatadir) \
	  -DV_MAJOR=${V_MAJOR} -DV_MINOR=${V_MINOR} -DV_BUILD=${V_BUILD} -DSANE_V_MAJOR=${SANE_V_MAJOR}"



AC_SUBST(V_MAJOR)
AC_SUBST(V_MINOR)
AC_SUBST(V_BUILD)
AC_SUBST(DLL_PRELOAD)

AC_ARG_ENABLE(foo1, [
Used environment variables that can be set by user:
  CFLAGS, CPPFLAGS, LDFLAGS, LIBS])

AC_ARG_ENABLE(foo2, [
To add include or library paths call:
  [env] CPPFLAGS=\"-I/path/to/foo/include\" LDFLAGS=\"-L/path/to/foo/libs\" ./configure])

AC_OUTPUT([Makefile libgtk/Makefile src/Makefile sanei/Makefile doc/Makefile 
           include/Makefile],)

echo "****************************************************************"
echo "*                                                              *"
echo "* SANE-FRONTENDS configure status:                             *"
echo "* --------------------------------                             *"

if test "x${HAVE_GTK}" != "x"; then
  echo "* - GUIs         activated (xcam + xscanimage)                 *"
  if test "${GIMP_LIBS}set" != "set"; then
    echo "* - GIMP plugin  activated for xscanimage                      *"
  else
    echo "* - GIMP plugin  deactivated for xscanimage                    *"
  fi
else
  if test "${USE_GUI}" = "yes"; then
    echo "* * GTK not found: GUIs deactivated                            *"
  else
    echo "* - GUIs         deactivated (xcam + xscanimage)               *"
  fi
fi

echo "*                                                              *"
echo "****************************************************************"
echo "*                                                              *"
echo "* To compile SANE-FRONTENDS:                                   *"
echo "* --------------------------                                   *"
echo "*   make                                                       *"
echo "*   make install                                               *"
echo "*                                                              *"
echo "* ------------------------------------------------------------ *"
echo "* ... PLEASE READ SANE DOCUMENTATION BEFORE STARTING SANE ...  *"
echo "* ------------------------------------------------------------ *"
echo "*                                                              *"
echo "* If you experience any problems with sane-frontends please    *"
echo "* write to the sane list at sane-devel@alioth-lists.debian.net *"
echo "*                                                              *"
echo "****************************************************************"
